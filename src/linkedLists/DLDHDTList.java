package linkedLists;

import java.util.Arrays;

public class DLDHDTList<E> implements LinkedList<E> {
	private DNode<E> header, trailer; 
	private int length; 
	
	public DLDHDTList() { 
		header = new DNode<E>();
		trailer = new DNode<E>();
		header.setNext(trailer);
		trailer.setPrev(header);
	}
	
	public void addFirstNode(Node<E> nuevo) {
		addNodeAfter(header, nuevo); 
	}

	public void addNodeAfter(Node<E> target, Node<E> nuevo) { //verify
		DNode<E> dnuevo = (DNode<E>) nuevo; 
		DNode<E> nodeBefore = (DNode<E>) target; 
		DNode<E> nodeAfter = nodeBefore.getNext(); 
		nodeBefore.setNext(dnuevo); 
		nodeAfter.setPrev(dnuevo); 
		dnuevo.setPrev(nodeBefore); 
		dnuevo.setNext(nodeAfter); 
		length++; 
	}

	public void addNodeBefore(Node<E> target, Node<E> nuevo) {
		DNode<E> dnuevo = (DNode<E>) nuevo; 
		DNode<E> nodeAfter = (DNode<E>) target;
		DNode<E> nodeBefore = nodeAfter.getPrev(); 
		nodeBefore.setNext(dnuevo); 
		nodeAfter.setPrev(dnuevo); 
		dnuevo.setPrev(nodeBefore); 
		dnuevo.setNext(nodeAfter); 
		length++; 
	}

	public Node<E> createNewNode() {
		return new DNode<E>();
	}

	public Node<E> getFirstNode() throws NodeOutOfBoundsException {
		if (length == 0) 
			throw new NodeOutOfBoundsException("List is empty."); 
		return header.getNext();
	}

	public Node<E> getLastNode() throws NodeOutOfBoundsException {
		if (length == 0) 
			throw new NodeOutOfBoundsException("List is empty."); 
		return trailer.getPrev();
	}

	public Node<E> getNodeAfter(Node<E> target)
			throws NodeOutOfBoundsException {
		if (length == 0) 
			throw new NodeOutOfBoundsException("List is empty.");
		return ((DNode<E>) target).getNext();
	}

	public Node<E> getNodeBefore(Node<E> target)
			throws NodeOutOfBoundsException {
		if (length == 0) 
			throw new NodeOutOfBoundsException("List is empty.");
		return ((DNode<E>) target).getPrev();
	}

	public int length() {
		return length;
	}

	public Node<E> removeFirstNode() throws NodeOutOfBoundsException {
		if (length == 0) 
			throw new NodeOutOfBoundsException("List is empty."); 
		DNode<E> ntd = header.getNext();
		header = ntd.getNext();
		ntd.getNext().setPrev(header);
		length--;
		ntd.cleanLinks();
		return ntd; 
	}

	public Node<E> removeLastNode() throws NodeOutOfBoundsException {
		if (length == 0) 
			throw new NodeOutOfBoundsException("List is empty."); 
		DNode<E> ntd = trailer.getPrev();
		trailer = ntd.getPrev(); 
		ntd.getPrev().setNext(trailer); 
		length--; 
		ntd.cleanLinks(); 
		return ntd; 
	}

	public void removeNode(Node<E> target) {
		DNode<E> ntd = (DNode<E>) target;
		ntd.getPrev().setNext(ntd.getNext());
		ntd.getNext().setPrev(ntd.getPrev());
		length--;
		ntd.cleanLinks();
	}
	
	/**
	* Prepares every node so that the garbage collector can free 
	* its memory space, at least from the point of view of the
	* list. This method is supposed to be used whenever the 
	* list object is not going to be used anymore. Removes all
	* physical nodes (data nodes and control nodes, if any)
	* from the linked list
	*/
	private void destroy() {
		while (header != null) { 
			DNode<E> nnode = header.getNext(); 
			header.setElement(null); 
			header.cleanLinks(); 
			header = nnode; 
		}
	}
	
	/**
	* The execution of this method removes all the data nodes from
	* the current instance of the list, leaving it as a valid empty
	* doubly linked list with dummy header and dummy trailer nodes. 
	*/
	public void makeEmpty() { 
	    this.destroy();
	}
		
	protected void finalize() throws Throwable {
	    try {
			this.destroy(); 
	    } finally {
	        super.finalize();
	    }
	}

	/**
	* Class to represent a node of the type used in doubly linked lists. 
	* @author pedroirivera-vega
	*
	* @param <E>
	*/
	private static class DNode<E> implements Node<E> {
		private E element; 
		private DNode<E> prev, next; 

		// Constructors
		public DNode() {}
		
		public DNode(E e) { 
			element = e; 
		}
		
		public DNode(E e, DNode<E> p, DNode<E> n) { 
			prev = p; 
			next = n; 
		}
		
		// Methods
		public DNode<E> getPrev() {
			return prev;
		}
		public void setPrev(DNode<E> prev) {
			this.prev = prev;
		}
		public DNode<E> getNext() {
			return next;
		}
		public void setNext(DNode<E> next) {
			this.next = next;
		}
		public E getElement() {
			return element; 
		}

		public void setElement(E data) {
			element = data; 
		} 
		
		/**
		* Just set references prev and next to null. Disconnect the node
		* from the linked list.... 
		*/
		public void cleanLinks() { 
			prev = next = null; 
		}
		
	}
	
	public Object[] toArray() {  					//remember to test it
		Object[] arr = new Object[length];
		for(int i = 0; i < length; i++) {
			arr[i] = this.getFirstNode().getElement();
			this.removeFirstNode();
		}
		return arr;
	}


	public <T1> T1[] toArray(T1[] array) {				//remember to test it
		if( array.length >= length) {
			for (int i = 0; i < length; i++) {
				array[i] = (T1) this.getFirstNode().getElement();
				this.removeFirstNode();
			}
			for (int j = length; j < array.length; j++) {
				array[j] = null;
			}
			return array;
		}
		else {
			T1[] arr = Arrays.copyOf(array, length);
			for(int i = 0; i < length; i++) {
				arr[i] = (T1) this.getFirstNode().getElement();
				this.removeFirstNode();
			}

			return arr;
	}

	}

}

